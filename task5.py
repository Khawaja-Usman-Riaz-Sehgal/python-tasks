#!/usr/bin/python

# Task 5
# You need to create 2 functions namely square(list) and cube(list),
# which takes the list as an argument and returns the list with all elements square and cube, respectively. 
# Then pass the list of 1st 10 natural numbers(X = [1, 2, 3 , 4, 5, 6, 7, 8, 9, 10]) to each function and
# plot the square and cube graphs/plots using matplotlib pyplot module.
# Note: If you have extra time, try labeling axis and give a suitable title to plots.

import matplotlib.pyplot as plt
def square(l1):
    l = list()
    for i in l1:
        l.append(i**2)
    print(l)
    return l

def cube(l1):
    l = list()
    for i in l1:
        l.append(i**3)
    print(l)
    return l

l1=[1,2,3,4,5,6,7,8,9,10]	

l=square(l1)
plot1 = plt. figure(1)
plt.plot(l1,l)
plt.title('Square Plot')
plt.xlabel('Natural Numbers')
plt.ylabel('Square of Natural Numbers')


l=cube(l1)
plot1 = plt. figure(2)
plt.plot(l1,l)
plt.title('Cube Plot')
plt.xlabel('Natural Numbers')
plt.ylabel('Cube of Natural Numbers')
plt.show()


