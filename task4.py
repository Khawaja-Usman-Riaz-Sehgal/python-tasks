#!/usr/bin/python

import random
import argparse
import json
import os

my_parser = argparse.ArgumentParser()
my_parser.add_argument('--i',help='write no of iterations')
my_parser.add_argument('--j',help='Load iterations from file',action='store_true')
args = my_parser.parse_args()

INTERVAL,pi=0,0

if args.j:
    f = open('j.json','r') 
    data = json.load(f) 
    for i in data:
        INTERVAL = data[i] 
elif args.i:
    INTERVAL= int(args.i)
else:
     os.system('python3 task4.py --h') 
circle_points= 0
square_points= 0
  

for i in range(INTERVAL**2): 
  
    rand_x= random.uniform(-1, 1) 
    rand_y= random.uniform(-1, 1) 
  
    # Distance between (x, y) from the origin 
    origin_dist= rand_x**2 + rand_y**2
  
    # Checking if (x, y) lies inside the circle 
    if origin_dist<= 1: 
        circle_points+= 1
  
    square_points+= 1
  
    # pi= 4*(no. of points generated inside the  
    # circle)/ (no. of points generated inside the square) 
    pi = 4* circle_points/ square_points 
  
print("Final Estimation of Pi=", pi)     

