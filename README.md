# Python-tasks

## Getting Started

To run this project following must be installed on your system.

### Prerequisites

- python3

```
 sudo apt-get update
 sudo apt-get install python3
```
- matplotlib 

```
pip3 install matplotlib
```
### Installation
- Clone the repo

`git clone https://gitlab.com/Khawaja-Usman-Riaz-Sehgal/python-tasks.git`

### Usage

- This project has six different tasks. All of them were written in python 3.

#### Task 1

- Write a program which will find all such numbers which are divisible by 7 but are not a multiple of 5, between 2000 and 3200 (both included). The numbers obtained should be printed in a comma-separated sequence on a single line.

- Run this code in your terminal using: `python3 /python-tasks/task1.py`
- Output will be like the following: 	![alt text](https://gitlab.com/Khawaja-Usman-Riaz-Sehgal/python-tasks/-/blob/master/task1.png)

#### Task 2

- The numbers after the direction are steps. Please write a program to compute the distance from the current position after a sequence of movement and original point. If the distance is a float, then just print the nearest integer. Use argparse library to take inputs for UP, DOWN, LEFT and RIGHT. Use of functions is encouraged.

Example: If the following tuples are given as input to the program:
UP 5
DOWN 3
LEFT 3
RIGHT 2
Then, the output of the program should be:
2

- Check the help first `python3 /python-tasks/task2.py --h`
- Run this code in your terminal using: `python3 /python-tasks/task2.py --flag flag`
- Output will be like the following: 	![alt text](https://gitlab.com/Khawaja-Usman-Riaz-Sehgal/python-tasks/-/blob/master/task2.png)

#### Task 3

- Suppose you are a hardware enthusiast and love checking system’s details. To make your task easy you have to write a program that is used to check hardware details of a system and generates a file for you; named “Summary.txt” at a location “/home/Username/Details”. If the directory “Details” does not exist on your system you have to create it. Details you are interested in include are given below along with example values. Remember you are not allowed to code in iPython. You can only use Python3 interpreter. Username will be the name of the user on your system for example “/home/engrhamza/Details”

|  | 
| ------ | 
| Byte Order:          Little Endian
Core(s) per socket:  4
Socket(s):           1
Model name:          Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
CPU MHz:             1638.462
CPU max MHz:         4000.0000
CPU min MHz:         400.0000
Virtualization Support:      VT-x
L1           32K
L2 cache:            256K
L3 cache:            8192K
RAM Memory: 15794MB |  


To make your problem easy you are allowed to add more details then the required one. 


- Run this code in your terminal using: `python3 /python-tasks/task3.py`
- Output will be like the following: 	![alt text](https://gitlab.com/Khawaja-Usman-Riaz-Sehgal/python-tasks/-/blob/master/Screenshot_from_2020-09-04_11-59-37.png)


#### Task 4

- You have to implement Monte Carlo’s simulation for finding the value of “pi”? You can look up Monte Carlo’s simulation on Google. You have to take the number of iterations from the user. User needs to pass the “-i” flag for entering the value of iterations. If he or she enters the “-h” flag then print the help of your tool explaining what it does and what are the possible inputs. If the “-j” flag is passed then your program has to read the value of “iterations” from a JSON file placed in the same directory your script is placed. Make sure you use the argparse library for flags and JSON format for the JSON file.

- Check the help first `python3 /python-tasks/task4.py --h`
- Run this code in your terminal using: `python3 /python-tasks/task4.py --flag flag`
- Output will be like the following: 	![alt text](https://gitlab.com/Khawaja-Usman-Riaz-Sehgal/python-tasks/-/blob/master/Screenshot_from_2020-09-04_12-08-31.png)


#### Task 5

- You need to create 2 functions namely square(list) and cube(list), which takes the list as an argument and returns the list with all elements square and cube, respectively. Then pass the list of 1st 10 natural numbers(X = [1, 2, 3 , 4, 5, 6, 7, 8, 9, 10]) to each function and plot the square and cube graphs/plots using matplotlib pyplot module.
Note: If you have extra time, try labeling axis and give a suitable title to plots.

- Run this code in your terminal using: `python3 /python-tasks/task5.py`
- Output will be like the following: 	![alt text](https://gitlab.com/Khawaja-Usman-Riaz-Sehgal/python-tasks/-/blob/master/Screenshot_from_2020-09-04_12-13-01.png)

#### Task 6

- You are given n words. Some words may repeat. For each word, output its number of occurrences. The output order should correspond with the input order of appearance of the word.

- Note: Each input line ends with a "\n" character.

###### Input Format

- The first line contains the integer, n .

- The next n lines each contain a word.

###### Output Format

- Output 2 lines. On the first line, output the number of distinct words from the input. On the second line, output the number of occurrences for each distinct word according to their appearance in the input.

- Run this code in your terminal using: `python3 /python-tasks/task6.py`
- Output will be like the following: 	![alt text](https://gitlab.com/Khawaja-Usman-Riaz-Sehgal/python-tasks/-/blob/master/Screenshot_from_2020-09-04_12-18-44.png)




