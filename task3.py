#!/usr/bin/python3

# Task 3
# Suppose you are a hardware enthusiast and love checking system’s details. 
# To make your task easy you have to write a program that is used to check hardware details of a system and generates a file for you; named “Summary.txt” at a location “/home/Username/Details”.
# If the directory “Details” does not exist on your system you have to create it. Details you are interested in include are given below along with example values.
# Remember you are not allowed to code in iPython. You can only use Python3 interpreter. Username will be the name of the user on your system for example “/home/engrhamza/Details”

# Byte Order:          Little Endian
# Core(s) per socket:  4
# Socket(s):           1
# Model name:          Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
# CPU MHz:             1638.462
# CPU max MHz:         4000.0000
# CPU min MHz:         400.0000
# Virtualization Support:      VT-x
# L1           32K
# L2 cache:            256K
# L3 cache:            8192K
# RAM Memory: 15794MB

# To make your problem easy you are allowed to add more details then the required one. 

import os
from pathlib import Path

home = str(Path.home())
dirName = home + '/' +'Details'

try:
    # Create target Directory
    os.mkdir(dirName)
    print("Directory " , dirName ,  " Created ") 
except FileExistsError:
    print("Directory " , dirName ,  " already exists")

os.system('lscpu > /home/emumba/Details/summary.txt')
os.system('cat /home/emumba/Details/summary.txt')


