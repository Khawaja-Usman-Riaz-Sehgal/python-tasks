#!/usr/bin/python


# Task 2
# The numbers after the direction are steps. Please write a program to compute the distance from the current position after a sequence of movement and original point. 
# If the distance is a float, then just print the nearest integer. Use argparse library to take inputs for UP, DOWN, LEFT and RIGHT. Use of functions is encouraged.
# Example: If the following tuples are given as input to the program:
# UP 5
# DOWN 3
# LEFT 3
# RIGHT 2
# Then, the output of the program should be:
# 2

import math
import argparse


def distance():
    x,y=0,0
    my_parser = argparse.ArgumentParser()
    my_parser.add_argument('--UP',help='write no of steps to move upwards',type=int)
    my_parser.add_argument('--DOWN',help='write no of steps to move downwards',type=int)
    my_parser.add_argument('--LEFT',help='write no of steps to move left',type=int)
    my_parser.add_argument('--RIGHT',help='write no of steps to move right',type=int)
    args = my_parser.parse_args()
    
    if args.UP:
        y=y+args.UP
    if args.DOWN:
        y-=args.DOWN
    if args.LEFT:
        x-=args.LEFT
    if args.RIGHT:
        x+=args.RIGHT
    return (x,y)

(x,y)=distance()
dist=round(math.sqrt(x**2+y**2))
print("Distance from current position to original Position = ", dist)