#!/usr/bin/python

#no of words to be entered
n = int(input())
#dictionary to save words.
dic  = {}

for _ in range(n):
    #input words 
    w = input()
    #check if word is already in the dictionary
    if w in dic.keys():
        #increment the value/count of the word
        dic[w]+=1
    else:
        #add new word with value 1 in dictionary
        dic.update({w:1})

#print no of words entered
print(len(dic.keys()))
#print occurences of words in order.
print(*dic.values())